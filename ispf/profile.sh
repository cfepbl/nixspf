#!/bin/bash

MAX=100

mkdir -p prof
for ((n=2; n <= MAX; n+=2))
do
	valgrind --tool=massif  --stacks=yes --massif-out-file=./prof/massif.out.$n  ./server -t2 -d1 -s1 -l20 -T12 4444 &
	sleep 1
	../fastclient/fastclient localhost 4444 $n > /dev/null
	sleep 1
done
for f in prof/massif.out.*;do echo -n ${${f}##*.},; ms_print $f | grep -oE "(.*)\^" | tr -d '^';done | sort -n<Paste>
