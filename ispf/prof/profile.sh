#!/bin/bash

MAX=100

for ((n=2; n <= MAX; n+=2))
do
	valgrind --tool=massif  --stacks=yes --massif-out-file=massif.out.$n  ../server -t2 -d1 -s1 -l20 -T12 4444 &
	sleep 1
	../../fastclient/fastclient localhost 4444 $n > /dev/null
	sleep 1
	#read -n 1 -s -r -p "Press any key to continue"
done
