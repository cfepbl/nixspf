/**COPY***************************************************************/
/*                                                                   */
/*  COPYRIGHT (C) 2019 ZACK WONG                                     */
/*  THIS PROGRAM IS FREE SOFTWARE: YOU CAN REDISTRIBUTE IT AND/OR    */
/*  MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS   */
/*  BIN PUBLISHED BY THE FREE SOFTWARE FOUNDATION, EITHER VERSION 3  */
/*  OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.           */
/*                                                                   */
/*  THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,  */
/*  BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF   */
/*  MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE    */
/*  GNU GENERAL PUBLIC LICENSE FOR MORE DETAILS.                     */
/*                                                                   */
/*  YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC        */
/*  LICENSE ALONG WITH THIS PROGRAM. IF NOT, SEE                     */
/*  <HTTPS://WWW.GNU.ORG/LICENSES/>.                                 */
/*                                                                   */
/*********************************************************************/
/**BRIEF**************************************************************/
/*                                                                   */
/*           MEMBER : MAIN            TYPE: C11SCE                   */
/*       PROGRAMMER : COFFEE          DATE: 28 JUL 2019              */
/*          CONTACT : <COFFEE@UCC.GU.UWA.EDU.AU>                     */
/*                                                                   */
/*      DESCRIPTION : ISPF MAIN MENU AND ENTRY POINT.                */
/*                                                                   */
/*********************************************************************/
/**OUTLINE************************************************************/
/*                                                                   */
/*********************************************************************/
/**MAINTAINANCE HISTORY***********************************************/
/*   DD MMM YYYY     NNNNNNNNN         <XXXXXXXXXXXXXXXXXXXXXXXXX>   */
/*   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   */
/*                                                                   */
/*********************************************************************/


/*********************************************************************/
/*                            INCLUDES                               */
/*********************************************************************/
#include "../../icpl/src/pane.h"
#include <stdbool.h>                     /* STDLIB BOOLEANS TYPE     */
#include <string.h>                      /* STDLIB STRING FUNCTIONS  */
#include <ctype.h>                       /* STDLIB TYPE UTILITY      */

/*********************************************************************/
/*                        PREPROCESSOR DEFS                          */
/*********************************************************************/

/*********************************************************************/
/*                        CONSTANT LITERALS                          */
/*********************************************************************/
#define TXTOPT "Option ===>"
#define TXTCMD "Command ===>"
#define TXTHDR "ISPF Primary Option Menu"
#define TXTQIT "Option X to terminate"

enum FLDNUM {
	BTN0,
	BTN1,
	BTN2,
	BTN3,
	BTN4,
	BTN5,
	INP0,
	OUT0
} ;

/*********************************************************************/
/*                     FUNCTION DECLARATIONS                         */
/*********************************************************************/
Screen* MAIN_A100_SUBMIT (Screen*);
void MAIN_B130_GET_NEXT_SCREEN (Screen*);
void MAIN_B120_DO_MENU_ACTION ();

/*********************************************************************/
/*                      LOCAL  DECLARATIONS                          */
/*********************************************************************/
Field MENUFLDS[] = { /* ISPF PRIMARY OPTION MENU */
 /*FLDID   | TYPE |ROW|COL|LEN|  TEXT       | ATTR     | NXT */
 /************************ MENU BAR **************************/
	[BTN0] = { BTN  ,  0,  3,  0,"Menu"       , A_NORMAL ,     },
  [BTN1] = { BTN  ,  0,  9,  0,"Utilities"  , A_NORMAL ,     },
  [BTN2] = { BTN  ,  0, 20,  0,"Compilers"  , A_NORMAL ,     },
  [BTN3] = { BTN  ,  0, 31,  0,"Options"    , A_NORMAL ,     },
  [BTN4] = { BTN  ,  0, 40,  0,"Status"     , A_NORMAL ,     },
  [BTN5] = { BTN  ,  0, 48,  0,"Help"       , A_NORMAL ,     },

 /************************* INPUTS ***************************/
  [INP0] = { INP  ,  3, 13, 66, ""          , INPT     ,     },

 /************************* LABELS ***************************/
  [OUT0] = { LBL  ,  3,  1,  0, ""          , TEXT(2)  ,     },
  /*AUTO*/ { LBL  ,  2, 28,  0,TXTHDR       , TEXT(4)  ,     },
  /*AUTO*/ { LBL  ,  3,  1,  0,TXTOPT       , TEXT(2)  ,     },
  /*AUTO*/ { LBL  , 20, 10,  0,TXTQIT       , TEXT(7)  ,     },

 /************************* RULERS ***************************/
  /*AUTO*/ { RLR,  1,  1,   78, "q"         , TEXT(4)  ,     },
  /*AUTO*/ {END}
};

/*********************************************************************/
/*                       WK_STORAGE                                  */
/*********************************************************************/
bool WK_IS_VALID;
Screen *WK_SCREEN;


/*********************************************************************/
/*    MAIN_A000_INIT                      							             */
/*********************************************************************/
#define MAIN_A000_INIT main
int MAIN_A000_INIT() {
	init();
	Screen *mainmenu = buildScreen(MENUFLDS);
  mainmenu->fstinp = &MENUFLDS[INP0];
  mainmenu->fstout = &MENUFLDS[OUT0];
	mainmenu->submit = MAIN_A100_SUBMIT;

  start(mainmenu);
  return 0;
} /* MAIN_A000_INIT */


/*********************************************************************/
/*    MAIN_MAINMENU_UPD                                              */
/*********************************************************************/



/*********************************************************************/
/*    MAIN_A100_SUBMIT                                               */
/*********************************************************************/
Screen* MAIN_A100_SUBMIT (Screen* self) {
  WK_SCREEN = self;
	int y, x;
	getyx(stdscr, y, x);



  Field *WK_CUR_FIELD = PANEL_X0100_GET_FLD(self,y,x);
  
  if (WK_CUR_FIELD->type == BTN)
    MAIN_B120_DO_MENU_ACTION();
  else
		MAIN_B130_GET_NEXT_SCREEN(self);

	return WK_SCREEN;
} /* MAIN_A100_SUBMIT */

/*********************************************************************/
/*    MAIN_B120_DO_MENU_ACTION                                       */
/*********************************************************************/
void MAIN_B120_DO_MENU_ACTION() {
  addch('*');
	return;

} /* MAIN_B120_DO_MENU_ACTION */


/*********************************************************************/
/*    MAIN_B130_GET_NEXT_SCREEN                                      */
/*********************************************************************/
void MAIN_B130_GET_NEXT_SCREEN (Screen *self){
  char* text = PANEL_U0100_GET_FIELD_TEXT(self->fstinp);
  for (int i = 0; i < strlen(text); ++i) {
     text[i] = toupper(text[i]);
  }
  char* token = strtok(text, " ");

  if (strcmp(token,"X") == 0)
    WK_SCREEN = NULL;
  // ADD OTHER OPTIONS HERE


	return;

} /* MAIN_B130_GET_NEXT_SCREEN */







