/**brief**************************************************************/
/*                                                                   */
/*         MEMBER:     PANE             TYPE: C11SCE                 */
/*         PROGRAMMER: COFFEE           <COFFEE@UCC.GU.UWA.EDU.AU>   */
/*                                                                   */
/*         PURPOSE:                                                  */
/*                                                                   */
/*********************************************************************/
/**OUTLINE************************************************************/
/*                                                                   */
/*********************************************************************/

/*********************************************************************/
/*                            INCLUDES                               */
/*********************************************************************/
#include "pane.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <curses.h>


/*********************************************************************/
/*                        CONSTANT LITERALS                          */
/*********************************************************************/
#define HGT 23
#define WDT 80

/*********************************************************************/
/*                     FUNCTION DECLARATIONS                         */
/*********************************************************************/
void draw(Screen *scr);

/*********************************************************************/
/*                      LOCAL  DECLARATIONS                          */
/*********************************************************************/
struct STATUS{
  bool isInsert;
  char *inhibit;
}status = {
  false,
  "",
};


/*********************************************************************/
/*               PANE_A100_DRAW                                      */
/*********************************************************************/
void draw(Screen *scr) {

  size_t oldy, oldx;

  getyx(stdscr, oldy, oldx);



  for (FNODE *node = scr->root ; node != NULL; node = node->next) {
    Field* fld = node->field;
    switch (fld->type) {

      case INP:

        move(fld->y, fld->x);

        attron(fld->attr);

        addstr(fld->text);

        for (int i = strlen(fld->text); i < fld->len; i++) addch(' ');

        break;

      case LBL:

        move(fld->y, fld->x);

        attron(fld->attr);

        addstr(fld->text);

        break;

      case BTN:
        move(0, fld->x);
        attron(TEXT(7));
        addstr(fld->text);
        mvchgat(0, fld->x + fld->len, 1, A_UNDERLINE, 7, NULL);
        break;
      case RLR:
        move(fld->y, fld->x);
        attron(fld->attr);
        hline(NCURSES_ACS(*fld->text),fld->len);
        break;
      default:
        break;

    }
    standend();
  }
  move(oldy, oldx);                 

}

void init() {
   initscr();
   noecho();

   cbreak();
   keypad(stdscr, true);
   curs_set(1);


   /* COLOUR INIT */
   start_color();
	 init_pair(8, COLOR_BLACK, COLOR_WHITE);
	 init_pair(1, COLOR_RED, COLOR_BLACK);
	 init_pair(2, COLOR_GREEN, COLOR_BLACK);
	 init_pair(3, COLOR_YELLOW, COLOR_BLACK);
	 init_pair(4, COLOR_BLUE, COLOR_BLACK);
	 init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
	 init_pair(6, COLOR_CYAN, COLOR_BLACK);
	 init_pair(7, COLOR_WHITE, COLOR_BLACK);
   /* END COLOUR INIT */

}

void draw_status() {
  int x, y;
  getyx(stdscr,y, x);

  attron(TEXT(2));
  mvhline(HGT,0,'_',80);
  mvaddstr(HGT + 1, 8, status.inhibit);
  mvaddstr(HGT + 1, 52, (status.isInsert ? "I" : "OVR"));
  mvprintw(HGT+1, 74, "%02i,%02i", y + 1, x + 1);
  standend();
	move(y,x);
	refresh();
}

/*********************************************************************/
/*    PANEL_X0100_GET_FLD                              							 */
/*********************************************************************/
Field* PANEL_X0100_GET_FLD(Screen* scr, int y, int x) {
  for (Field *fd = scr->fstinp; fd != NULL; fd = fd->nxfld) {
    if (fd->y == y && (x >= fd->x && x < fd->x + fd->len )) {
      return fd;
    }
  }

  return NULL;
} /* PANEL_X0100_GET_FLD */

/*********************************************************************/
/*    PANEL_X0200_PUTCHAR                             							 */
/*********************************************************************/
Field* PANEL_X0200_PUTCHAR(Screen* scr, int y, int x, char ch) {
  Field *fd = PANEL_X0100_GET_FLD(scr, y, x);
  if (fd) {
    attron(fd->attr);
    addch(ch);
    standend();
    if (x == fd->x + fd->len -1 ) {
    if (fd->nxfld)
      move(fd->nxfld->y, fd->nxfld->x);
    else 
      move (y,x);
    }
  }

  return fd;
} /* PANEL_X0200_PUTCHAR */

/*********************************************************************/
/*    PANEL_X0300_GO_HOME                              							 */
/*********************************************************************/
void PANEL_X0300_GO_HOME(Screen* scr, int *y, int *x) {
  Field *fd = PANEL_X0100_GET_FLD(scr, *y, *x);
  if (fd) {
    *y = fd->y;
    *x = fd->x;
  }
} /* PANEL_X0300_GO_HOME */

/*********************************************************************/
/*    PANEL_X0400_GO_END                               							 */
/*********************************************************************/
void PANEL_X0400_GO_END(Screen* scr, int *y, int *x) {
  Field *fd = PANEL_X0100_GET_FLD(scr, *y, *x);
  if (fd) {
    *y = fd->y;
    *x = fd->x + fd->len -1;
  }
} /* PANEL_X0400_GO_END */


/*********************************************************************/
/*    PANEL_X0300_BUILD_SCREEN                         							 */
/*********************************************************************/
Screen* buildScreen(Field *fields) {
  Screen* scr;
  scr = (Screen*) calloc(1, sizeof(Screen));
  int i = 0;
  FNODE *prev = NULL;
  while (fields[i].type != END) {
    FNODE *node = (FNODE*) malloc(sizeof (FNODE));
    node->field = &fields[i++];
    if (scr->root == NULL) {
      scr->root = node;
      prev = node;
    }
    else {
      prev->next = node;
			prev = node;
    }

  }

  return scr;
} /* PANEL_X0200_BUILD_SCREEN */

/*********************************************************************/
/*    PANEL_U0100_GET_FIELD_TEXT                       							 */
/*********************************************************************/
char* PANEL_U0100_GET_FIELD_TEXT(Field *fld) {
  char *buf = malloc(WDT + 1);
  int x, y;
  getyx(stdscr,y, x);
  mvinstr(fld->y, fld->x, buf);
  move(y,x);
  return buf;
} /* PANEL_U0100_GET_FIELD_TEXT */


int start(Screen *scr) {

  init();


  draw(scr);
  int x, y;
  move(3, 13);
  getyx(stdscr,y, x);
  draw_status();
  int chr = mvinch(y,x); // save attr
  chtype attr = chr & A_ATTRIBUTES;
  chgat(1, A_REVERSE | attr, 0, NULL); // set block

  while (1) {

    int ch = getch();

    switch (ch) {

      case KEY_LEFT:

        if (x > 0)

          x--;

        break;

      case KEY_RIGHT:

        if (x < 79)

          x++;

        break;

      case KEY_UP:

        if (y > 0)

          y--;

        break;

      case KEY_DOWN:

        if (y < HGT - 1)

          y++;

        break;

      case KEY_HOME:

        PANEL_X0300_GO_HOME(scr, &y, &x);
        break;  
      case KEY_END:

        PANEL_X0400_GO_END(scr, &y, &x);
        break;  
      case KEY_STAB:

      case KEY_BTAB:

        break;  
			case 10:
			case KEY_ENTER:
				status.inhibit = "X - SYSTEM";
				draw_status();

				scr = scr->submit(scr);
        if(!scr)
          goto exit;

				status.inhibit = "          ";
				draw_status();

			 	break;
      default:
        if (isprint(ch)) {
         if(PANEL_X0200_PUTCHAR(scr, y, x, ch)){
            getyx(stdscr,y, x);
            goto redraw;}
        }
        break;

    }
    addch(chr);
redraw:
    chr = mvinch(y,x); // save attr
    attr = chr & A_ATTRIBUTES;
    chgat(1, A_REVERSE | attr, 0, NULL);
    draw_status();
  }
exit:
  endwin();
  return 0;
}
