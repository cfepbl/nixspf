/**COPY***************************************************************/
/*                                                                   */
/*  COPYRIGHT (C) 2019 ZACK WONG                                     */
/*  THIS PROGRAM IS FREE SOFTWARE: YOU CAN REDISTRIBUTE IT AND/OR    */
/*  MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS   */
/*  BIN PUBLISHED BY THE FREE SOFTWARE FOUNDATION, EITHER VERSION 3  */
/*  OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.           */
/*                                                                   */
/*  THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,  */
/*  BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF   */
/*  MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE    */
/*  GNU GENERAL PUBLIC LICENSE FOR MORE DETAILS.                     */
/*                                                                   */
/*  YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC        */
/*  LICENSE ALONG WITH THIS PROGRAM. IF NOT, SEE                     */
/*  <HTTPS://WWW.GNU.ORG/LICENSES/>.                                 */
/*                                                                   */
/*********************************************************************/
/**BRIEF**************************************************************/
/*                                                                   */
/*           MEMBER : PANE            TYPE: C11HDR                   */
/*       PROGRAMMER : COFFEE          DATE: 28 JUL 2019              */
/*          CONTACT : <COFFEE@UCC.GU.UWA.EDU.AU>                     */
/*                                                                   */
/*      DESCRIPTION : ISPF MAIN MENU AND ENTRY POINT.                */
/*                                                                   */
/*********************************************************************/
/**OUTLINE************************************************************/
/*                                                                   */
/*********************************************************************/
/**MAINTAINANCE HISTORY***********************************************/
/*   DD MMM YYYY     NNNNNNNNN         <XXXXXXXXXXXXXXXXXXXXXXXXX>   */
/*   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   */
/*                                                                   */
/*********************************************************************/
#ifndef PANE_H
#define PANE_H

/*********************************************************************/
/*                            INCLUDES                               */
/*********************************************************************/
#include <curses.h>
#include <stdint.h>
#include <stddef.h>
#include <malloc.h>
#include <string.h>
#include <stdbool.h>

/*********************************************************************/
/*                        PREPROCESSOR DEFS                          */
/*********************************************************************/
#define TEXT(c) A_PROTECT | COLOR_PAIR(c)
#define INPT A_UNDERLINE | COLOR_PAIR(6)

/*********************************************************************/
/*                        CONSTANT LITERALS                          */
/*********************************************************************/
#define LBL 0
#define BTN 1
#define RLR 2
#define INP 3
#define END 0b1111

/*********************************************************************/
/*                         TYPE DEFINITIONS                          */
/*********************************************************************/
typedef unsigned char byte;

typedef struct Field {
  byte type : 4;
  size_t y  : 16
       , x  : 16
       , len;
  char* text;
  chtype attr;
  struct Field *nxfld;
  void* (*callback)(char*);
} Field;

typedef struct FNODE {
  int offset;
  Field *field;
  struct FNODE *next;
} FNODE;

typedef struct Screen {
  Field* fstout;
  Field* fstinp;
  FNODE* root;

	bool update;
  struct Screen* (*submit)(struct Screen*);
}Screen;


/*********************************************************************/
/*                            EXPORTS                                */
/*********************************************************************/
extern void init();
extern int start(Screen* scr);
Field* PANEL_X0100_GET_FLD(Screen* scr, int y, int x);
extern char* PANEL_U0100_GET_FIELD_TEXT(Field *fld);
extern Screen* buildScreen(Field *fields);
#endif
